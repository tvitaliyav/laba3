# USAGE
# python real_time_object_detection.py --prototxt MobileNetSSD_deploy.prototxt.txt --model MobileNetSSD_deploy.caffemodel

# import the necessary packages
from imutils.video import FPS
import numpy as np
# import argparse
import imutils
import time
import cv2
from PIL import Image as im


'''
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", required=True,
                help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
                help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.2,
                help="minimum probability to filter weak detections")
args = vars(ap.parse_args())
'''
# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))
Image = 'Histogram.png'

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe("MobileNetSSD_deploy.prototxt.txt", "MobileNetSSD_deploy.caffemodel")

# initialize the video stream, allow the cammera sensor to warmup,
# and initialize the FPS counter
print("[INFO] starting video stream...")

#video_name = 'carscut.mp4'
video_name = 'lab.mp4'

vs = cv2.VideoCapture(video_name)
assert not isinstance(vs, type(None)), 'video not found'
print(vs)

print("[INFO] starting image process")
time.sleep(2.0)
fps = FPS().start()

while not vs.isOpened():
    vs = cv2.VideoCapture(video_name)
    print("Wait for the header")

# loop over the frames from the video stream
cnt = 0

# task_3
area_start_wight = 200
area_end_wight = 700
area_start_height = 100
area_end_height = 450

# task_8
imageColors = np.zeros((506, 900, 3), np.uint8)

while vs.isOpened():
    cnt = cnt + 1
    obj_cnt = 0
    empty_frames_flag = False
    # grab the frame from the threaded video stream and resize it
    # to have a maximum width of 400 pixels
    ret, frame = vs.read()
    frame = imutils.resize(frame, width=900)

    if frame.dtype == None:
        fps.stop()
        print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
        print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

    # grab the frame dimensions and convert it to a blob
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (720, 720)), 0.007843, (720, 720), 0)

    # pass the blob through the network and obtain the detections and
    # predictions
    net.setInput(blob)
    detections = net.forward()

    cars_counter = 0
    people_counter = 0
    in_area_cnt = 0
    # loop over the detections

    N_cnt = 0

    for i in np.arange(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with
        # the prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence > 0.2:

            N_cnt = N_cnt + 1
            empty_frames_flag = True
            # extract the index of the class label from the
            # `detections`, then compute the (x, y)-coordinates of
            # the bounding box for the object
            idx = int(detections[0, 0, i, 1])
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            imageColors[startY:endY, startX:endX, 0] = imageColors[startY:endY, startX:endX, 0] + 10
            if ((area_start_wight < startX) & (area_end_wight > endX) & (area_start_height < startY) & (
                    area_end_height > endY)):
                label = "{}: {:.2f}% HERE WE ARE".format(CLASSES[idx], confidence * 100)
                in_area_cnt = in_area_cnt + 1
            else:
                label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)

            # draw the prediction on the frame
            cv2.rectangle(frame, (startX, startY), (endX, endY), COLORS[idx], 2)

            y = startY - 15 if startY - 15 > 15 else startY + 15
            cv2.putText(img=frame,
                        text=label,
                        org=(startX, y),
                        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale=0.5,
                        color=COLORS[idx],
                        thickness=2)

            if label.startswith('car'):
                cars_counter += 1
            if label.startswith('person'):
                people_counter += 1

    # point3
    cv2.putText(img=frame,
                text='Objects in area now counter: {in_area_cnt}'.format(in_area_cnt=in_area_cnt),
                org=(5, 15),
                fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=0.5,
                color=(255, 255, 255),
                thickness=1)

    # write the output frame ---------------------
    # out_file.write(frame)
    # --------------------------------------------

    # show the output frame
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

    # update the FPS counter
    fps.update()
    if vs.get(cv2.cv2.CAP_PROP_POS_FRAMES) == vs.get(cv2.cv2.CAP_PROP_FRAME_COUNT):
        # If the number of captured frames is equal to the total number of frames,
        # we stop
        break
# stop the timer and display FPS information
fps.stop()
data = im.fromarray(imageColors)
data.save(Image)
print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
vs.release()
# out_file.release()
cv2.destroyAllWindows()
